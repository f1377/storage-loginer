# Storage Loginer [2022]

Creating a User Login with Flutter Secure Storage dependency.

## Dependencies
- [flutter_secure_storage](https://pub.dev/packages/flutter_secure_storage)

## App Overview

### Login Screen

![Login Screen](/images/readme/login_screen.png "Login Screen")

### Register Screen

![Register Screen](/images/readme/register_screen.png "Register Screen")

### Home Screen

![Home Screen](/images/readme/home_screen.png "Home Screen")
