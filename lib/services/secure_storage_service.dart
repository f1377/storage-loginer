import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SecureStorageService {
  final FlutterSecureStorage _storageService = const FlutterSecureStorage();

  Future<void> writeIntoStorage(String key, String value) async {
    await _storageService.write(key: key, value: value);
  }

  Future<void> setIsLoggedIn() async {
    await _storageService.write(key: 'isLoggedIn', value: 'true');
  }

  Future<void> setIsNotLoggedIn() async {
    await _storageService.write(key: 'isLoggedIn', value: 'false');
  }

  Future<bool> isLoggedIn() async {
    final String? isLoggedIn = await _storageService.read(key: 'isLoggedIn');

    if (isLoggedIn != null &&
        isLoggedIn.isNotEmpty &&
        isLoggedIn.contains('true')) {
      return true;
    }

    return false;
  }

  Future<void> readFromStorage(String key) async {
    final String? storageEntry = await _storageService.read(key: key);
    print("storageEntry: ${storageEntry}");
  }

  Future<void> readAllFromStorage() async {
    final Map<String, String> storage = await _storageService.readAll();
    for (var entry in storage.entries) {
      print("key: ${entry.key}");
      print("value: ${entry.value}");
    }
  }

  Future<String?> getValueFromStorageKey(String key) async {
    final storageEntry = await _storageService.read(key: key);

    return storageEntry;
  }

  Future<void> deleteAll() async {
    await _storageService.deleteAll();
  }
}
