import 'package:flutter/material.dart';
import 'package:storage_loginer/screens/home_screen.dart';
import 'package:storage_loginer/screens/register_screen.dart';
import 'package:storage_loginer/services/secure_storage_service.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final SecureStorageService _storageService = SecureStorageService();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    _storageService.readAllFromStorage();
    super.initState();
  }

  Future<void> _onLogin() async {
    if (!_formKey.currentState!.validate()) {
      return;
    }

    final String? storedUsername =
        await _storageService.getValueFromStorageKey('username');
    final String? storedPassword =
        await _storageService.getValueFromStorageKey('password');

    if (storedUsername == null || storedPassword == null) {
      showSnackBarMessage('Error: Please Register first!');
      return;
    }

    if (_usernameController.text != storedUsername ||
        _passwordController.text != storedPassword) {
      showSnackBarMessage('Error: Username or Password incorrect!');
      return;
    }

    await _storageService.setIsLoggedIn();
    _navigateToHomeScreen();
  }

  void showSnackBarMessage(String message) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text(message)));
  }

  void _navigateToHomeScreen() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => const HomeScreen(),
      ),
    );
  }

  void _onRegister() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => RegisterScreen(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                margin: const EdgeInsets.only(top: 40.0),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.blueGrey, width: 2.0),
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(50.0),
                    bottomRight: Radius.circular(25.0),
                  ),
                ),
                child: const Icon(
                  Icons.leaderboard,
                  size: 124.0,
                  color: Colors.blueGrey,
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(
                  horizontal: 16.0,
                ),
                child: Column(
                  children: [
                    TextFormField(
                      controller: _usernameController,
                      decoration: const InputDecoration(
                        hintText: 'Enter a Username',
                        labelText: 'Username',
                      ),
                      validator: (String? value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter a valid Username';
                        }

                        return null;
                      },
                    ),
                    TextFormField(
                      controller: _passwordController,
                      decoration: const InputDecoration(
                        hintText: 'Enter a Password',
                        labelText: 'Password',
                      ),
                      obscureText: true,
                      autocorrect: false,
                      enableSuggestions: false,
                      validator: (String? value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter a valid Password';
                        }

                        return null;
                      },
                    ),
                  ],
                ),
              ),
              Container(
                height: 50.0,
                width: double.infinity,
                margin: const EdgeInsets.symmetric(horizontal: 25.0),
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.blueGrey, width: 2.0),
                    borderRadius: BorderRadius.circular(10.0)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: ElevatedButton(
                        onPressed: _onLogin,
                        child: const Text('Login'),
                      ),
                    ),
                    Container(
                      height: 50.0,
                      width: 50.0,
                      margin: const EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.blueGrey, width: 2.0),
                        borderRadius: const BorderRadius.only(
                          topRight: Radius.circular(100.0),
                          bottomLeft: Radius.circular(100.0),
                        ),
                      ),
                    ),
                    Expanded(
                      child: ElevatedButton(
                        onPressed: _onRegister,
                        child: const Text('Register'),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
