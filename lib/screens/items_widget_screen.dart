import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:flutter/foundation.dart';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter/material.dart';
import 'package:storage_loginer/models/select_item_model.dart';
import 'package:storage_loginer/widgets/edit_item_widget.dart';

enum _Actions { deleteAll }
enum _ItemActions { delete, edit, containsKey }

class ItemsWidgetScreen extends StatefulWidget {
  const ItemsWidgetScreen({Key? key}) : super(key: key);

  @override
  _ItemsWidgetScreenState createState() => _ItemsWidgetScreenState();
}

class _ItemsWidgetScreenState extends State<ItemsWidgetScreen> {
  final _storage = const FlutterSecureStorage();
  final _accountNameController =
      TextEditingController(text: 'flutter_secure_storage_service');

  List<SecItem> _items = [];
  String? _getAccountName() =>
      _accountNameController.text.isEmpty ? null : _accountNameController.text;

  Future<void> _addNewItem() async {
    final String key = _randomValue();
    final String value = _randomValue();

    await _storage.write(
        key: key,
        value: value,
        iOptions: _getIOSOptions(),
        aOptions: _getAndroidOptions());
    _readAll();
  }

  Future<void> _deleteAll() async {
    await _storage.deleteAll(
        iOptions: _getIOSOptions(), aOptions: _getAndroidOptions());
    _readAll();
  }

  Future<void> _performAction(
      _ItemActions action, SecItem item, BuildContext context) async {
    switch (action) {
      case _ItemActions.delete:
        await _storage.delete(
            key: item.key,
            iOptions: _getIOSOptions(),
            aOptions: _getAndroidOptions());
        _readAll();

        break;
      case _ItemActions.edit:
        final result = await showDialog<String>(
            context: context, builder: (context) => EditItemWidget(item.value));
        if (result != null) {
          await _storage.write(
              key: item.key,
              value: result,
              iOptions: _getIOSOptions(),
              aOptions: _getAndroidOptions());
          _readAll();
        }
        break;
      case _ItemActions.containsKey:
        final result = await _storage.containsKey(key: item.key);
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text('Contains Key: $result'),
          // backgroundColor: Colors.green,
          duration: const Duration(seconds: 4),
        ));
        break;
    }
  }

  Future<void> _readAll() async {
    final all = await _storage.readAll(
        iOptions: _getIOSOptions(), aOptions: _getAndroidOptions());
    setState(() {
      _items = all.entries
          .map((entry) => SecItem(entry.key, entry.value))
          .toList(growable: false);
    });
  }

  String _randomValue() {
    final rand = Random();
    final codeUnits = List.generate(20, (index) {
      return rand.nextInt(26) + 65;
    });

    return String.fromCharCodes(codeUnits);
  }

  IOSOptions _getIOSOptions() => IOSOptions(
        accountName: _getAccountName(),
      );

  AndroidOptions _getAndroidOptions() => const AndroidOptions(
        encryptedSharedPreferences: true,
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Plugin example app'),
        actions: <Widget>[
          IconButton(
              key: const Key('add_random'),
              onPressed: _addNewItem,
              icon: const Icon(Icons.add)),
          PopupMenuButton<_Actions>(
              key: const Key('popup_menu'),
              onSelected: (action) {
                switch (action) {
                  case _Actions.deleteAll:
                    _deleteAll();
                    break;
                }
              },
              itemBuilder: (BuildContext context) => <PopupMenuEntry<_Actions>>[
                    const PopupMenuItem(
                      key: Key('delete_all'),
                      value: _Actions.deleteAll,
                      child: Text('Delete all'),
                    ),
                  ])
        ],
      ),
      body: Column(
        children: [
          if (!kIsWeb && Platform.isIOS)
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: TextFormField(
                controller: _accountNameController,
                decoration: const InputDecoration(labelText: 'kSecAttrService'),
              ),
            ),
          Expanded(
            child: ListView.builder(
              itemCount: _items.length,
              itemBuilder: (BuildContext context, int index) => ListTile(
                trailing: PopupMenuButton(
                    key: Key('popup_row_$index'),
                    onSelected: (_ItemActions action) =>
                        _performAction(action, _items[index], context),
                    itemBuilder: (BuildContext context) =>
                        <PopupMenuEntry<_ItemActions>>[
                          PopupMenuItem(
                            value: _ItemActions.delete,
                            child: Text(
                              'Delete',
                              key: Key('delete_row_$index'),
                            ),
                          ),
                          PopupMenuItem(
                            value: _ItemActions.edit,
                            child: Text(
                              'Edit',
                              key: Key('edit_row_$index'),
                            ),
                          ),
                          PopupMenuItem(
                            value: _ItemActions.containsKey,
                            child: Text(
                              'Contains Key',
                              key: Key('contains_row_$index'),
                            ),
                          ),
                        ]),
                title: Text(
                  _items[index].value,
                  key: Key('title_row_$index'),
                ),
                subtitle: Text(
                  _items[index].key,
                  key: Key('subtitle_row_$index'),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
