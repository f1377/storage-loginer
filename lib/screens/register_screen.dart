import 'package:flutter/material.dart';
import 'package:storage_loginer/screens/login_screen.dart';
import 'package:storage_loginer/services/secure_storage_service.dart';

class RegisterScreen extends StatelessWidget {
  RegisterScreen({Key? key}) : super(key: key);

  final SecureStorageService _storageService = SecureStorageService();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  Future<void> _onSubmit(BuildContext context) async {
    if (!_formKey.currentState!.validate()) {
      return;
    }

    await _storeDataIntoStorage();
    _navigateToLoginScreen(context);
  }

  Future<void> _storeDataIntoStorage() async {
    await _storageService.writeIntoStorage(
        'username', _usernameController.text);
    await _storageService.writeIntoStorage('email', _emailController.text);
    await _storageService.writeIntoStorage(
        'password', _passwordController.text);
  }

  void _navigateToLoginScreen(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => const LoginScreen(),
      ),
    );
  }

  void _onCancel(BuildContext context) {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text('Please Register'),
        ),
        automaticallyImplyLeading: false,
      ),
      body: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
                children: [
                  Card(
                    child: ListTile(
                      leading: const Icon(Icons.person),
                      iconColor: Colors.white,
                      tileColor: Colors.blueGrey.shade400,
                      textColor: Colors.white,
                      title: TextFormField(
                        controller: _usernameController,
                        cursorColor: Colors.white,
                        style: const TextStyle(color: Colors.white),
                        decoration: const InputDecoration(
                          hintText: "Enter a Username",
                          hintStyle: TextStyle(color: Colors.white),
                          labelText: "Username",
                          labelStyle: TextStyle(color: Colors.white),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white70),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          errorBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.orangeAccent),
                          ),
                          focusedErrorBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.orangeAccent),
                          ),
                          errorStyle: TextStyle(
                            color: Colors.orangeAccent,
                          ),
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a valid Username';
                          }

                          return null;
                        },
                      ),
                    ),
                  ),
                  Card(
                    child: ListTile(
                      leading: const Icon(Icons.email),
                      iconColor: Colors.white,
                      tileColor: Colors.blueGrey.shade400,
                      textColor: Colors.white,
                      title: TextFormField(
                        controller: _emailController,
                        cursorColor: Colors.white,
                        style: const TextStyle(color: Colors.white),
                        decoration: const InputDecoration(
                          hintText: "Enter a Email",
                          hintStyle: TextStyle(color: Colors.white),
                          labelText: "Email",
                          labelStyle: TextStyle(color: Colors.white),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white70),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          errorBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.orangeAccent),
                          ),
                          focusedErrorBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.orangeAccent),
                          ),
                          errorStyle: TextStyle(
                            color: Colors.orangeAccent,
                          ),
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a valid Email';
                          }

                          if (!RegExp(r'\S+@\S+\.\S+').hasMatch(value)) {
                            return 'Please enter a valid Email!';
                          }

                          return null;
                        },
                      ),
                    ),
                  ),
                  Card(
                    child: ListTile(
                      leading: const Icon(Icons.password),
                      iconColor: Colors.white,
                      tileColor: Colors.blueGrey.shade400,
                      textColor: Colors.white,
                      title: TextFormField(
                        controller: _passwordController,
                        cursorColor: Colors.white,
                        style: const TextStyle(color: Colors.white),
                        decoration: const InputDecoration(
                          hintText: "Enter a Password",
                          hintStyle: TextStyle(color: Colors.white),
                          labelText: "Password",
                          labelStyle: TextStyle(color: Colors.white),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white70),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          errorBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.orangeAccent),
                          ),
                          focusedErrorBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.orangeAccent),
                          ),
                          errorStyle: TextStyle(
                            color: Colors.orangeAccent,
                          ),
                        ),
                        autocorrect: false,
                        obscureText: true,
                        enableSuggestions: false,
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a valid Password';
                          }

                          return null;
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 50.0,
              width: double.infinity,
              margin: const EdgeInsets.symmetric(horizontal: 25.0),
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: ElevatedButton(
                      onPressed: () => _onSubmit(context),
                      child: const Text('Register'),
                    ),
                  ),
                  Container(
                    height: 50.0,
                    width: 50.0,
                    margin: const EdgeInsets.all(8.0),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.blueGrey, width: 2.0),
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(100.0),
                        bottomRight: Radius.circular(100.0),
                      ),
                    ),
                  ),
                  Expanded(
                    child: ElevatedButton(
                      onPressed: () => _onCancel(context),
                      child: const Text('Cancel'),
                      style: ElevatedButton.styleFrom(primary: Colors.red),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
