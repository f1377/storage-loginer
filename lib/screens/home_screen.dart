import 'package:flutter/material.dart';
import 'package:storage_loginer/services/secure_storage_service.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final SecureStorageService _storageService = SecureStorageService();

  String? _username;
  String? _password;
  String? _email;

  @override
  void initState() {
    super.initState();
    _setVariables();
  }

  Future<void> _setVariables() async {
    _username = await _storageService.getValueFromStorageKey('username');
    _password = await _storageService.getValueFromStorageKey('password');
    _email = await _storageService.getValueFromStorageKey('email');

    setState(() {});
  }

  Future<void> _onLogout() async {
    await _storageService.setIsNotLoggedIn();
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(child: Text("Welcome")),
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            onPressed: _onLogout,
            icon: const Icon(Icons.logout),
          ),
        ],
      ),
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(8.0),
          width: 400.0,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Card(
                child: ListTile(
                  leading: const Icon(Icons.person),
                  title: Text(
                    _username ?? "-",
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  iconColor: Colors.white,
                  tileColor: Colors.blueGrey,
                  textColor: Colors.white,
                  subtitle: const Text(
                    "Username",
                    style: TextStyle(fontSize: 12.0),
                  ),
                ),
              ),
              Card(
                child: ListTile(
                  leading: const Icon(Icons.email),
                  title: Text(
                    _email ?? "-",
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  iconColor: Colors.white,
                  tileColor: Colors.blueGrey,
                  textColor: Colors.white,
                  subtitle: const Text(
                    "Email",
                    style: TextStyle(fontSize: 12.0),
                  ),
                ),
              ),
              Card(
                child: ListTile(
                  leading: const Icon(Icons.password),
                  title: Text(
                    _password ?? "-",
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  iconColor: Colors.white,
                  tileColor: Colors.blueGrey,
                  textColor: Colors.white,
                  subtitle: const Text(
                    "Password",
                    style: TextStyle(fontSize: 12.0),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class TextWidget extends StatelessWidget {
  const TextWidget({
    Key? key,
    required String? textName,
    required String? textValue,
  })  : _textName = textName,
        _textValue = textValue,
        super(key: key);

  final String? _textName;
  final String? _textValue;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(color: Colors.black54),
        ),
      ),
      child: Text(
        "$_textName: $_textValue",
        style: const TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 24.0,
        ),
      ),
    );
  }
}
